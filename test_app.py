from fastapi.testclient import TestClient
from app import app

client = TestClient(app)

def test_root():
  response = client.get("/")
  assert response.status_code == 200

def test_posts():
  response = client.get("/posts")
  assert response.status_code == 200

