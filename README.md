# Fast API crud

## 🧩 Variables de entorno

```bash
# DB
MY_DB_USER=docker
MY_DB_PASS=docker
MY_DB_NAME=dbname
MY_DB_HOST=localhost
MY_DB_PORT=3306
ENVIRONMENT=development # development or production
# Keycloak
KC_ENDPOINT=https://endpoint.here/protocol/openid-connect
KC_SECRET=TheSecretKeyForRealm
KC_CLIENT=NameForClient
```

## 📦 Instalar dependencias

```bash
$ pip install -r requirements.txt
```

## 🚀 Iniciar el proyecto

```bash
$ uvicorn app:app
```

# Desarrollo

```Nota:``` Mantener el proyecto activo para escuchar los cambios requiere el parámetro --reload
```bash
$ uvicorn app:app --reload
```

## 🧑🏻‍💻 Entorno virtual para el proyecto (anaconda/conda)

Crea el entorno virtual de desarrollo
```bash
$ conda create --name project-name python=3
```

Activa entorno virtual
```bash
$ conda activate project-name
```

Desactiva entorno virtual
```bash
$ conda deactivate
```

```Nota:``` Si el interprete no es reconocido por VSC, es posible forzarlo desde la paleta de comandos (⌘ + p) y seleccionar la versión asociada a *conda*

```
> python select interpreter
```

## 📦 Instalación de paquetes necesarios

```bash
$ pip install fastapi uvicorn
```

```Nota:``` Para preservar la referencia de los paquetes instalados requiere ejecutar el siguiente comando, cada vez que se añade un nuevo paquete al proyecto

```bash
$ pip freeze > requeriments.txt
```

## ⚙️ Pruebas unitarias

```
$ pytest
```

### Enlaces

- [🐍 FastAPI](https://fastapi.tiangolo.com/)
- [📗 Swagger](https://swagger.io/)
- [🐍 anaconda](https://anaconda.com/products/individual)
- [📦 code reference](https://github.com/FaztWeb/fastapi-restapi-crud)
- [📹 Guide 1](https://www.youtube.com/watch?v=_eWEmRWhk9A)
- [📹 Guide 2](https://www.youtube.com/watch?v=6eVj33l5e9M)