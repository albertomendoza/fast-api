from fastapi.security import HTTPBearer
from fastapi import APIRouter, Response, status, Depends
from uuid import uuid4 as uuid
from config.db import connection
from models.post import posts
from schemas.post import Post
from starlette.status import HTTP_204_NO_CONTENT

# from cryptography.fernet import Fernet
# key = Fernet.generate_key()
# f = Fernet(key)

# Get bearer token
# https://t.ly/Lo7z
# https://t.ly/8B0D
token = HTTPBearer()

post = APIRouter(
    prefix="/v1",
    tags=["posts"],
    dependencies=[Depends(token)],
    responses={ 404: { "status": False } }
)

@post.get('/')
def read_root():
    return { "mensaje": "hola mundo" }

@post.get('/posts', response_model=list[Post], tags=["posts"])
def get_posts():
    return connection.execute(posts.select()).fetchall()

@post.post('/post', response_model=Post, tags=["posts"])
def save_post(post: Post):
    new_post = { 
        "id": str(uuid()), 
        "title": post.title, 
        "author": post.author, 
        "content": post.content, 
        "published": post.publised,
        "published_at": post.published_at,
        "created_at": post.created_at
    }
    connection.execute(posts.insert().values(new_post))
    return new_post

@post.get('/posts/{post_id}', response_model=Post, tags=["posts"])
def get_post(post_id: str):
    return connection.execute(posts.select().where(posts.c.id == post_id)).first()
    
@post.delete('/posts/{post_id}', status_code=status.HTTP_204_NO_CONTENT, tags=["posts"])
def delete_post(post_id: str):
    result = connection.execute(posts.delete().where(posts.c.id == post_id))
    return Response(status_code=HTTP_204_NO_CONTENT)

@post.put('/posts/{post_id}', response_model=Post, tags=["posts"])
def update_post(post_id: str, post: Post):
    connection.execute(posts.update().values(
        title=post.title, 
        author=post.author, 
        content=post.content, 
        published=post.publised
        ).where(posts.c.id == post_id))
    return connection.execute(posts.select().where(posts.c.id == post_id)).first()