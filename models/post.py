from sqlalchemy import Table, Column
from sqlalchemy.sql.sqltypes import Integer, String, Text, Boolean, DateTime
from config.db import meta, engine

posts = Table(
    "posts",
    meta,
    Column("id", String(255), primary_key=True),
    Column("title", String(255)),
    Column("author", String(255)),
    Column("content", Text()),
    Column("published", Boolean(False)),
    Column("created_at", DateTime()), 
    Column("published_at", DateTime()))

meta.create_all(engine)
