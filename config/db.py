from sqlalchemy import create_engine, MetaData
import os

USER = os.getenv("MY_DB_USER", "docker")
PASS = os.getenv("MY_DB_PASS", "docker")
NAME = os.getenv("MY_DB_NAME", "docker")
HOST = os.getenv("MY_DB_HOST", "localhost")
PORT = os.getenv("MY_DB_PORT", "5432")

engine = create_engine("postgresql://"+USER+":"+PASS+"@"+HOST+":"+PORT+"/"+NAME)
meta = MetaData()
connection = engine.connect()