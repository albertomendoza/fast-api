import requests
from fastapi import FastAPI, Request, Header
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from routes.post import post
import os

# Constants
ENV = os.getenv("environment", default="production")
KC_ENDPOINT = os.getenv("KC_ENDPOINT", default="default-endpoint")
KC_SECRET = os.getenv("KC_SECRET", default="default-secret")
KC_CLIENT = os.getenv("KC_CLIENT", default="default-client")

app = FastAPI(
  docs_url="/docs" if (ENV == "development") else None,
  title="Posts API",
  description="Breve descripción del API",
  version="0.5.0"
)

# Middleware for different routes
# https://t.ly/0ioD2

app.add_middleware(
  CORSMiddleware,
  allow_origins=['*'],
  allow_credentials=True,
  allow_methods=['*'],
  allow_headers=['*']
)

@app.middleware("http")
async def get_token(request: Request, call_next):
  response = await call_next(request)
  if (ENV == "development"): return response
  if 'Authorization' in request.headers:
    token = request.headers.get('Authorization')
  else:
    return JSONResponse({ "status": False, "message": "Unauthorized" }, status_code=401)

  validate = await verify_token(token)
  if (validate == False): return JSONResponse({ "status": False, "message": "Invalid token" }, status_code=401)
  return response

async def verify_token(token):
  payload = {
    "client_secret": KC_SECRET,
    "client_id": KC_CLIENT,
    "token": token.replace('Bearer ', '')
  }
  introspect = KC_ENDPOINT+"/token/introspect"
  validate = requests.post(introspect, data=payload)
  return validate.json()['active']

app.include_router(post)