from pydantic import BaseModel, Field
from typing import Text, Optional
from datetime import datetime

class Post(BaseModel):
    title: str = Field(
        ...,
        min_length=1,
        max_length=50,
        example="Un super título desde la configuración"
    )
    author: str
    content: Text
    created_at: datetime = datetime.now()
    published_at: Optional[datetime]
    publised: Optional[bool] = False